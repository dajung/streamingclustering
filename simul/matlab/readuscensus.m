function datainput = readuscensus(filename, M, D)

    fid         = fopen(filename, 'rb');
    datastream  = fread(fid, Inf, 'uint8');
    datastream  = datastream-48;

    fclose(fid);
    N     = size(datastream);
    s = 1;
    t = 1;
    tmp = 0;
    sign = 1;
    count = 1;

    for i = 1:N    
        in = datastream(i); 
        if (in == -38) 
            t = t+1;
            s = 1;
        else
            if ((in == -4)||(in == -35))
                datainput(t,s) = uint16(tmp*sign);
                count = count+1;
                if(count == M*D) 
                    break;
                end
                tmp = 0;
                sign = 1;
                s = s+1;
            else
                if (in == -3)
                    sign = -1;
                else
                    tmp = tmp*10+in;
                end
            end
        end
    end

    end