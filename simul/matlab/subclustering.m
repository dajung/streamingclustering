function [centroids, cost] = subclustering(inputdata, centroids, learning_rate)

    [M, N] = size(inputdata);
    [K, D] = size(centroids);
    
    cost = 0;
    %clusters = zeros(M,1);

    for i = 1:M

        input  = inputdata(i,:);
        distance = pdist2(centroids, input, 'cityblock');

       
        [mind,minidx] = min(distance);            

        centroids(minidx,:) = centroids(minidx,:)*(1-learning_rate) + input*learning_rate;
        %clusters(i) = minidx;
        cost = cost+mind;

    end

    cost = cost/M;

end