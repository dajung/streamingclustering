function [cluster, cost] = clustering_nearest(inputdata, centroids)

    [M, N] = size(inputdata);
    [K, D] = size(centroids);
    cluster = zeros(M,1);
    cost = 0 ;

    for i = 1:M

        input  = inputdata(i,:);
        distance = pdist2(centroids, input, 'cityblock');

        [mind,minidx] = min(distance);            

        cluster(i) = minidx;
        cost = cost+mind;

    end

    cost = cost/M;

end