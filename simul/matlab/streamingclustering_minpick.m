%
%  Created by Dajung Lee on 3/31/17.
%  Copyright � 2017 Dajung Lee. All rights reserved.
%

close all; clear all;

DATASET = 4;     % choose between 0 and 6
SHUFFLE = 1;     % a flag for shuffling on/off
RATE    = 0.125; % learning rate, \alpha, to determine the step size

switch DATASET
    case 0 % ------------- Cell data 9D
        K = 10;
        D = 9;
        N = 16384; % a single image
        inputfilename = '../../data/celldata9D.input.dat';
        initfilename  = '../../data/celldata9D.init.dat';
        fid = fopen(inputfilename, 'r');
        inputdata = (fscanf(fid, '%d', [D,Inf])');
        fclose(fid);
        fid = fopen(initfilename, 'r');
        centerdata = (fscanf(fid, '%f', [D,Inf])');
        fclose(fid); 
        inputdatatmp = inputdata;
        inputdata = inputdatatmp(N+1:N*2,:);
    case 1 % ------------- cell data 1D
        K = 10;
        D = 1;
        N = 16384;
        %F = 2;
        inputfilename = '../../data/celldata1D.input.dat';
        initfilename  = '../../data/celldata1D.init.dat';
        fid = fopen(inputfilename, 'r');
        inputdata = (fscanf(fid, '%d', [D,Inf])');
        fclose(fid);
        fid = fopen(initfilename, 'r');
        centerdata = (fscanf(fid, '%f', [D,Inf])');
        fclose(fid);  
        inputdatatmp = inputdata;
        inputdata = inputdatatmp(N+1:N*2,:);    
    case 2 % ------------- US Census
        K = 10;
        D = 68;
        N = 2457600;
        inputfilename = '../../data/uscensus1990.input.dat';
        initfilename  = '../../data/uscensus1990.init.dat';
        datainput = fileparsingus(inputfilename, N, D+1);
        fid = fopen(initfilename, 'r');
        centerdata = (fscanf(fid, '%f', [D,Inf])');
        fclose(fid); 
        %load centerdataUS;
    case 3 % ------------- Spamdata
        K = 10;
        D = 58;
        inputfilename = '../../data/spambase.input.dat';
        initfilename  = '../../data/spambase.init.dat';
        fid = fopen(inputfilename, 'r');
        inputdata = (fscanf(fid, '%d', [D,Inf])');
        fclose(fid);
        fid = fopen(initfilename, 'r');
        centerdata = (fscanf(fid, '%f', [D,Inf])');
        fclose(fid); 
        %load centerdataspam;
    case 4 % ------------- circles
        K = 2; 
        D = 2;
        inputfilename = '../../data/circles.input.dat';
        fid = fopen(inputfilename, 'r');
        inputdata = (fscanf(fid, '%f', [D,Inf])');
        fclose(fid);
        centerdata = inputdata(1:K, :);
    case 5 % ------------- moons  
        K = 2; 
        D = 2;
        inputfilename = '../../data/moons.input.dat';
        fid = fopen(inputfilename, 'r');
        inputdata = (fscanf(fid, '%f', [D,Inf])');
        fclose(fid);
        centerdata = inputdata(1:K, :);
    case 6 % ------------- blobs
        K = 3; 
        D = 2;
        inputfilename = '../../data/blobs.input.dat';
        fid = fopen(inputfilename, 'r');
        inputdata = (fscanf(fid, '%f', [D,Inf])');
        fclose(fid);
        centerdata = inputdata(1:K, :);
end
%%

[N, M]        = size(inputdata);

% Shuffling input
if (SHUFFLE == 1)
    inputdata   = shuffling(inputdata);
end

% Different initial seeding points
if (DATASET < 2) 
    % for cell images in 9D and 1D
    centerdata3 = centerdata(      1:K, :);
    centerdata2 = centerdata(  K+1:2*K, :);
    centerdata4 = centerdata(2*K+1:3*K, :);
    centerdata5 = centerdata(3*K+1:4*K, :);
    centerdata1 = centerdata(4*K+1:5*K, :);
elseif (DATASET > 3)             
    % for 2D synthetic data : circles, moons, blobs
    K1 = K;
    [~,centerdata1] = kmeans(inputdata(1:100, :), K1); 
    [~,centerdata2] = kmeans(inputdata(1:100, :), K1); 
    [~,centerdata3] = kmeans(inputdata(1:100, :), K1); 
    [~,centerdata4] = kmeans(inputdata(1:100, :), K1); 
    [~,centerdata5] = kmeans(inputdata(1:100, :), K1); 
else  
    % for big data sets : spambase, uscensus1990
    centerdata1 = centerdata;
    centerdata2 = centerdata;
    centerdata3 = centerdata;
    centerdata4 = centerdata;
    centerdata5 = centerdata;
end

%% stage 1. streaming subclustering

[centers_s1, cost_s1] = subclustering(inputdata, centerdata1, RATE);
[centers_s2, cost_s2] = subclustering(inputdata, centerdata2, RATE/2);
[centers_s3, cost_s3] = subclustering(inputdata, centerdata3, RATE/4);
[centers_s4, cost_s4] = subclustering(inputdata, centerdata4, RATE/8);
[centers_s5, cost_s5] = subclustering(inputdata, centerdata5, RATE/16);

%% stage 2. reduction - minimum cost pick

[cost, index]  = min([cost_s1, cost_s2, cost_s3, cost_s4, cost_s5]);
centers_s = [centers_s1, centers_s2, centers_s3, centers_s4, centers_s5];
centers_s = centers_s(:, (index-1)*D+1:index*D);

%% stage 3. clustering based on centers_s

if ((DATASET < 2)&&(SHUFFLE))
    inputdata = inputdatatmp(N+1:N*2,:);
end

[cluster_s, cost_s1] = clustering_nearest(inputdata, centers_s);

%% stage 4. (post-processing) visualization. (cell images and 2D synthetic data only)

% Segmentation

if (DATASET < 2) 
    % for cell images in 9D and 1D
    normal_factor = floor(256/K);
    frame_s = reshape(cluster_s, [128, 128]);
    histogram_final = histogram(frame_s);
    pdf = histogram_final.Values;

    segmentation_s = frame_s;
    sum = 0 ;

    while (sum < (16384*.9))
        [n, c] = max(pdf);
        segmentation_s(segmentation_s == c) = 0;
        pdf(c) = 0;
        sum = sum+n;
    end
    figure,
    subplot(1, 2, 1), imshow(ind2rgb(frame_s*normal_factor, jet(256)));
    subplot(1, 2, 2), imshow(segmentation_s);
elseif (DATASET > 3)
    % for 2D synthetic data : circles, moons, blobs
    scatter(inputdata(:,1), inputdata(:,2), [], cluster_s, 'filled');
end

