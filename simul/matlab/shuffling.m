function output = shuffling(input)

    output = input(randperm(size(input,1)), :);
 
end